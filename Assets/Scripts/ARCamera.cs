﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

public class ARCamera : MonoBehaviour {
    float _yRot;
    UnityARSessionNativeInterface _session;
    Camera _cam;
    bool _sessionStarted;
    void Start() {
        _cam = GetComponent<Camera>();
        _session = UnityARSessionNativeInterface.GetARSessionNativeInterface();
        UnityARSessionNativeInterface.ARFrameUpdatedEvent += FirstFrameUpdate;
    }
    void Update() {
        if (!_sessionStarted) return;
        _cam.projectionMatrix = _session.GetCameraProjection();
        Matrix4x4 matrix = _session.GetCameraPose();
        _cam.transform.localRotation = UnityARMatrixOps.GetRotation(matrix);
        _cam.transform.localPosition = UnityARMatrixOps.GetPosition(matrix);
    }
    void FirstFrameUpdate(UnityARCamera cam) {
        _sessionStarted = true;
        UnityARSessionNativeInterface.ARFrameUpdatedEvent -= FirstFrameUpdate;
    }
}
