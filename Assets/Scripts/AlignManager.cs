﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

public class AlignManager : MonoBehaviour {
    public Camera _camera;
    public LayerMask _hideLayers;
    public GameObject _ui;

    UnityARSessionNativeInterface session {
        get {
            if (_session == null) {
                _session = UnityARSessionNativeInterface.GetARSessionNativeInterface();
            }
            return _session;
        }
    }
    UnityARSessionNativeInterface _session;
    ARTrackingState _trackingState;

    LayerMask _layers;
    void Start() {
        _layers = _camera.cullingMask;
        SetupAR();
        LoseAlignment();
    }
    void Update() {

    }
    public void LoseAlignment() {
        _camera.cullingMask = _hideLayers;
        _ui.SetActive(true);
    }
    public void GetAlignment() {
        _camera.cullingMask = _layers;
        _ui.SetActive(false);
    }
    void SetupAR() {
        UnityARSessionNativeInterface.ARFrameUpdatedEvent += TrackARState;
    }

    void ClearAR() {
        UnityARSessionNativeInterface.ARFrameUpdatedEvent -= TrackARState;
    }

    void TrackARState(UnityARCamera cam) {
        _trackingState = cam.trackingState;
        if (_trackingState == ARTrackingState.ARTrackingStateLimited || _trackingState == ARTrackingState.ARTrackingStateNotAvailable) {
            LoseAlignment();
        }
    }

    void OnDestroy() {
        ClearAR();
    }
}