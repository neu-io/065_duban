﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlphaX : MonoBehaviour {
    public GameObject reference;
    public float offset = 0;
    Material _mat;
    void Start() {
        _mat = GetComponent<MeshRenderer>().material;
    }
    void Update() {
        _mat.SetFloat("_Opacity", Mathf.Abs(reference.transform.localPosition.x) - offset);
    }
}
