﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonActive : MonoBehaviour {
    public GameObject relation;
    public Color highlight;

    Image _image;
    Color _startColor;
    void Awake() {
        _image = GetComponent<Image>();
        _startColor = _image.color;
    }
    void Update() {
        var state = relation.activeInHierarchy;
        _image.color = state ? highlight : _startColor;
    }
}
