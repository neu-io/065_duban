﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetPositioning : MonoBehaviour {
    public Transform _transform;
    public Text _x;
    public Text _y;
    public Text _z;
    public Text _a;
    public Text _b;
    public Text _g;

    void Update() {
        var pos = _transform.localPosition;
        _x.text = "x:" + (pos.x).ToString("F2");
        _y.text = "y:" + (pos.y).ToString("F2");
        _z.text = "z:" + (pos.z).ToString("F2");
        var rot = _transform.localEulerAngles;
        _a.text = "a:" + (rot.x).ToString("F2");
        _b.text = "b:" + (rot.y).ToString("F2");
        _g.text = "g:" + (rot.z).ToString("F2");
    }
}
