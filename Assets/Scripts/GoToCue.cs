﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using NEEEU.Baadi;

public class GoToCue : MonoBehaviour {
    PlayableDirector _director;
    void Start() {
        _director = GetComponent<PlayableDirector>();
        if (_director == null) {
            Destroy(this);
            Debug.Log("No Playable Director");
            return;
        }
    }

    public void Go(string cueName) {
        var tracks = _director.playableAsset as TimelineAsset;
        foreach (var track in tracks.GetOutputTracks()) {
            var cueTrack = track as BaadiTrack;
            if (cueTrack == null) {
                continue;
            }
            foreach (var clips in cueTrack.GetClips()) {
                var cue = clips.asset as BaadiClip;
                if (cue == null || cue.cueLabel != cueName) {
                    continue;
                }
                _director.time = clips.start;
                _director.Play();
            }
        }
    }
}
