﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageOrient : MonoBehaviour {
    public Sprite portrait;
    public Sprite landscape;

    Image ui;
    void Start() {
        ui = GetComponent<Image>();
        if (portrait == null) {
            portrait = ui.sprite;
        }
        if (landscape == null) {
            landscape = ui.sprite;
        }
    }
    void Update() {
        ui.sprite = Input.deviceOrientation == DeviceOrientation.Portrait || Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown ? portrait : landscape;
    }
}
