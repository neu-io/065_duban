﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpBetween : MonoBehaviour {
    public Transform _target;
    [Range(0, 1)]
    public float _lerp;
    Vector3 _origPosition;
    Quaternion _origRotation;
    Vector3 _origScale;

    void Start() {
        _origPosition = transform.position;
        _origRotation = transform.rotation;
        _origScale = transform.localScale;
    }

    void Update() {
        transform.position = Vector3.Lerp(_origPosition, _target.position, _lerp);
        transform.rotation = Quaternion.Lerp(_origRotation, _target.rotation, _lerp);
        transform.localScale = Vector3.Lerp(_origScale, _target.localScale, _lerp);
    }
}
