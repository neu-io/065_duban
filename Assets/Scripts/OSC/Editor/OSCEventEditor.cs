using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(OSCEvent))]
public class OSCEventEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var myTarget = (OSCEvent)target;
        if (GUILayout.Button("Trigger!"))
        {
            myTarget.Trigger();
        }
        base.OnInspectorGUI();

    }
}