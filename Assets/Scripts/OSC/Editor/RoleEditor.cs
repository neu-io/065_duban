using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(RoleManager))]
public class RoleEditor : Editor {
    public override void OnInspectorGUI() {
        var myTarget = (RoleManager)target;
        myTarget.currentRole = (RoleManager.Role)EditorGUILayout.EnumPopup("Current Role", myTarget.currentRole);
        base.OnInspectorGUI();

    }
}