﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class OSCEvent : MonoBehaviour, IPointerClickHandler {
    public UnityEvent OnTrigger;

    public OscOut oscOut;

    public void Trigger() {
        var path = transform.GetPath();
        path = path.Replace(' ', '_');
        var message = new OscMessage(path);

        var id = System.Guid.NewGuid().ToString();
        message.Add(id);

        StartCoroutine(SendMessage(message));
    }

    public void OnPointerClick(PointerEventData data) {
        Trigger();
    }

    IEnumerator SendMessage(OscMessage message) {
        for (int i = 0; i < 10; i++) {
            oscOut.Send(message);
            yield return null;
        }
    }

    List<string> handledEvents = new List<string>();
    public void HandleEvent(string id) {
        if (handledEvents.Contains(id)) {
            return;
        }
        handledEvents.Add(id);
        OnTrigger.Invoke();
        Debug.Log("Triggered: " + id);
    }
}
