﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[ExecuteInEditMode]
public class OSCManager : MonoBehaviour {
    [Required]
    public OscIn oscIn;
    [Required]
    public OscOut oscOut;

    void Start() {
        var events = Resources.FindObjectsOfTypeAll<OSCEvent>();
        foreach (var eventCmp in events) {
            eventCmp.oscOut = oscOut;
            var path = eventCmp.transform.GetPath();
            path = path.Replace(' ', '_');
            oscIn.UnmapAll(path);

            var action = new UnityAction<string>(eventCmp.HandleEvent);
            oscIn.MapString(path, action);
        }
    }

    void OnApplicationFocus(bool focusStatus) {
        oscIn.Open(oscIn.port, oscIn.multicastAddress);
        oscOut.Open(oscOut.port, oscOut.ipAddress);
    }
}