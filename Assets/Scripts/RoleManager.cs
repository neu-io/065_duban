﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class RoleManager : MonoBehaviour {

    public enum Role {
        Admin,
        Player
    }
    public Role role;
    public Role currentRole {
        get {
            return (Role)PlayerPrefs.GetInt("admin", 1);
        }
        set {
            PlayerPrefs.SetInt("admin", (int)value);
        }
    }
    void Awake() {

        int prefrole = PlayerPrefs.GetInt("admin", 1);
        if (prefrole != (int)role) gameObject.SetActive(false);
    }
}
