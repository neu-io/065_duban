﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateAround : MonoBehaviour {
    public float target;
    public float speed;
    public Button button;

    float cur;
    public void Rotate() {
        target += 90;
    }

    void Start() {
        target = transform.localEulerAngles.y;
        cur = transform.localEulerAngles.y;
    }

    void Update() {
        var rot = transform.localEulerAngles;
        // rot.y = Mathf.Lerp(rot.y, target, speed * Time.deltaTime);
        // rot.y += Mathf.Sign(rot.y - target) * speed * Time.deltaTime;
        var dif = speed * Time.deltaTime;
        if (target - cur < dif) {
            cur = target;
            button.interactable = true;
        }
        else {
            button.interactable = false;
            if (target > cur) {
                cur += dif;

            }
            if (target < cur) {
                cur -= dif;
            }
        }
        rot.y = cur;
        transform.localEulerAngles = rot;
    }
}