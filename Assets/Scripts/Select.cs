﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Select : MonoBehaviour {
    public GameObject defaultActive;
    public List<GameObject> _options;
    GameObject _current;

    void Start() {
        if (!Application.isPlaying) {
            return;
        }
        foreach (var obj in _options) {
            obj.SetActive(false);
        }
        defaultActive.SetActive(true);
        _current = defaultActive;
    }
    void Update() {
        if (_options == null) return;
        foreach (var obj in _options) {
            if (obj == null) continue;
            if (obj.activeSelf && obj != _current) {
                _current = obj;
                foreach (var obj2 in _options) {
                    if (obj2 == obj) {
                        continue;
                    }
                    obj2.SetActive(false);
                }
            }
        }
    }
}
